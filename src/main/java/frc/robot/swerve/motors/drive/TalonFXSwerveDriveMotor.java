package frc.robot.swerve.motors.drive;

import com.ctre.phoenix6.controls.VelocityVoltage;
import com.ctre.phoenix6.hardware.TalonFX;

import frc.robot.Constants;
import frc.robot.util.Conversions;

public class TalonFXSwerveDriveMotor extends SwerveDriveMotor {
    private final TalonFX motor;
    
    private final VelocityVoltage velocityControl;

    public TalonFXSwerveDriveMotor(int canId) {
        motor = new TalonFX(canId);
        velocityControl = new VelocityVoltage(0);
    }

    @Override
    public void set(double speed) {
        motor.set(speed);
    }

    @Override
    public void setVelocity(double rpm, double feedfoward) {
        motor.setControl(velocityControl.withVelocity(Conversions.RPMToRPS(rpm)).withFeedForward(feedfoward));
    }

    @Override
    public void setLinearVelocity(double mps, double feedforward) {
        double velocity = Conversions.MPSToRPS(mps, Constants.Swerve.WHEEL_CIRCUMFERENCE);
        motor.setControl(
            velocityControl.withVelocity(velocity).withFeedForward(feedforward));
    }

    @Override
    public double getVelocity() {
        return Conversions.RPSToRPM(motor.getVelocity().getValue());
    }

    @Override
    public double getLinearVelocity() {
        return Conversions.RPSToMPS(motor.getVelocity().getValue(), Constants.Swerve.WHEEL_CIRCUMFERENCE);
    }

    @Override
    public double getPosition() {
        return motor.getPosition().getValue();
    }

    @Override
    public double getDisplacement() {
        return Conversions.rotationsToMeters(motor.getPosition().getValue(), Constants.Swerve.WHEEL_CIRCUMFERENCE);
    }

    @Override
    public void config() {
        motor.getConfigurator().apply(Constants.Swerve.CTRE_CONFIGS.swerveDriveConfig); 
        motor.setInverted(Constants.Swerve.INVERT_DRIVE_MOTORS);
        motor.setNeutralMode(Constants.Swerve.DRIVE_NEUTRAL_MODE);
        motor.setPosition(0);
    }

    @Override
    public TalonFX getMotor() {
        return motor;
    }
    
}
