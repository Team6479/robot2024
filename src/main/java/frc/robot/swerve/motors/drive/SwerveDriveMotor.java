// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.swerve.motors.drive;

import edu.wpi.first.wpilibj.motorcontrol.MotorController;

public abstract class SwerveDriveMotor {
    public SwerveDriveMotor() {}

    /**
     * Runs a motor with percent ouput/duty cycle
     * @param speed Percent output [-1, 1]
     */
    public abstract void set(double speed);

    /**
     * Sets the target velocity of the mechanism in RPM
     * @param rpm Target velocity in RPM
     */
    public void setVelocity(double rpm) {
        setVelocity(rpm, 0);
    }

    /**
     * Sets the target velocity of the mechanism in RPM
     * @param rpm Target velocity in RPM
     * @param feedfoward Arbitrary feedfoward to add
     */
    public abstract void setVelocity(double rpm, double feedforward);

    /**
     * Sets the target velocity of the mechanism required to reach mps meters per second
     * @param mps Target velocity in meters per second
     */
    public void setLinearVelocity(double mps) {
        setLinearVelocity(mps, 0);
    }

    /**
     * Sets the target velocity of the mechanism required to reach mps meters per second
     * @param mps Target velocity in meters per second
     * @param feedfoward Arbitrary feedfoward to add
     */
    public abstract void setLinearVelocity(double mps, double feedforward);

    /**
     * Get the velocity of the mechanism in RPM
     * @return Current velocity in RPM
     */
    public abstract double getVelocity();

    /**
     * Get the linearized velocity of the mechanism in meters per second
     * @return Current velocity in meters per second
     */
    public abstract double getLinearVelocity();

    /**
     * Get the position of the mechanism in rotations
     * @return Rotations
     */
    public abstract double getPosition();

    /**
     * Get the displacement from the number of rotations of the wheel
     * @return Meters
     */
    public abstract double getDisplacement();

    public abstract void config();

    public abstract MotorController getMotor();
}
