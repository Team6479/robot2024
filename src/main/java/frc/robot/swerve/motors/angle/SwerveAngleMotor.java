// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.swerve.motors.angle;

import edu.wpi.first.wpilibj.motorcontrol.MotorController;

public abstract class SwerveAngleMotor {
    public SwerveAngleMotor() {}

    /**
     * Runs a motor with percent ouput/duty cycle
     * @param speed Percent output [-1, 1]
     */
    public abstract void set(double speed);

    /**
     * Sets the target position of the mechanism in rotations
     * @param rotations Target position in rotations
     */
    public abstract void setPosition(double rotations);

    /**
     * Sets the position that the sensor reads
     * @param rotations Position in rotations
     */
    public abstract void setSensorPosition(double rotations);

    /**
     * Get the position of the mechanism in rotations
     * @return Rotations
     */
    public abstract double getPosition();

    public abstract void config();

    public abstract MotorController getMotor();
}
