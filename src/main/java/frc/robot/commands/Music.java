// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import java.util.ArrayList;
import com.ctre.phoenix6.Orchestra;
import com.ctre.phoenix6.hardware.TalonFX;
import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.subsystems.Swerve;
import frc.robot.swerve.SwerveModule;

public class Music extends Command {
    /** Creates a new Music. */
    private ArrayList<TalonFX> motors = new ArrayList<TalonFX>();

    /** List all songs in playlist */
    private String[] songs = new String[] {
        "AngryBirds.chrp",
        "AztecFightSong.chrp",
        "PvZtest.chrp",
        "GeometryDash.chrp",
        "CabinetMan.chrp",
        "SweetChildOfMine.chrp",
        "BoulevardOfBrokenDreams.chrp",
        "AbandonShip.chrp",
        "UptownFunk.chrp",
        "Meglalovania.chrp",
        "GravityFallsTheme.chrp",
        "AmericanIdiot.chrp",
    };

    /** Number of songs on playlist */ 
    private int playlist = songs.length -1;
    private int selectedSong = 0;
    private Orchestra[] orchestras = new Orchestra[playlist];
    
    public Music(Swerve swerve) {

        // addRequirements(swerve);

        for(SwerveModule module : swerve.modules) {
            motors.add((TalonFX)module.getAngleMotor().getMotor());
            motors.add((TalonFX)module.getDriveMotor().getMotor());
        }

        for (int i = 0; i < playlist; i++) {
            orchestras[i] = new Orchestra();
            // orchestras[i].addInstrument(motors.get(i));
            for (TalonFX motor : motors) {
                orchestras[i].addInstrument(motor);
            }
        }
    }

    public void nextSong() {
        selectedSong += 1;
        if(selectedSong > playlist){
            selectedSong = 0;
        }
    }

    public void previousSong() {
        selectedSong -= 1;
        if(selectedSong < 0){
            selectedSong = playlist;
        }
    }

    public void pauseSong() {
        for (var orchestra : orchestras) {
            orchestra.pause();
        }
    }

    public void playSong() {
        for (var orchestra : orchestras) {
            orchestra.play();
        }
    }

    public void stopMusic() {
        for (var orchestra : orchestras) {
            orchestra.stop();
        }
    }

    /** Called when the command is initially scheduled. */ 
    @Override
    public void initialize() {
        for (var orchestra : orchestras) {
            orchestra.loadMusic(songs[selectedSong]);
        }
        playSong();
    }

    /** Called every time the scheduler runs while the command is scheduled. */
    @Override
    public void execute() {}

    /** Called once the command ends or is interrupted. */ 
    @Override
    public void end(boolean interrupted) {}

    /** Returns true when the command should end. */ 
    @Override
    public boolean isFinished() {
        return false;
    }
}
